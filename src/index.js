import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { Auth0Client } from './auth0/auth0-spa-js';
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Auth0Provider } from "./auth0/auth0-react";
import history from "./utils/history";
import { getConfig } from "./config";

const onRedirectCallback = (appState) => {
  history.push(
    appState && appState.returnTo ? appState.returnTo : window.location.pathname
  );
};

// Please see https://auth0.github.io/auth0-react/interfaces/auth0_provider.auth0provideroptions.html
// for a full list of the available properties on the provider
const config = getConfig();

const providerConfig = {
  domain: config.domain,
  clientId: config.clientId,
  ...(config.audience ? { audience: config.audience } : null),
  redirectUri: window.location.origin,
  onRedirectCallback,
};

// Copied from node_modules/@auth0/auth0-react/src/auth0-provider.tsx
const toAuth0ClientOptions = (opts) => {
  const { clientId, redirectUri, maxAge, ...validOpts } = opts;
  return {
    ...validOpts,
    client_id: clientId,
    redirect_uri: redirectUri,
    max_age: maxAge,
  };
};

const Auth0Wrapper = ({ children }) => {
  const [client] = useState(
    () => new Auth0Client(toAuth0ClientOptions(providerConfig))
  );

  // For debugging
  useEffect(() => {
    window.auth0Client = client;
  }, [client]);

  return (
    <Auth0Provider {...providerConfig}>
      {children}
    </Auth0Provider>
  );
};

ReactDOM.render(
  <Auth0Wrapper>
    <App />
  </Auth0Wrapper>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
