const { messageHandler } = jest.requireActual('../token.worker');

export default class {
  postMessage(data: any, ports: any) {
    messageHandler({
      data,
      ports
    });
  }
}
